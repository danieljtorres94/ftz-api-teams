import { FastifyRequest, FastifyReply } from 'fastify';
import { isHttpError } from 'http-errors'
import { ValidationError } from 'joi';
import logger from '../logger';

// const errors = [
//   {
//     code: 'BAD_REQUEST',
//     status: 400,
//     message: 'Invalid params'
//   }
// ];

export default (error: any, request: FastifyRequest, reply: FastifyReply) => {
  const errorObject = {
    status: 500,
    code: 'DEFAULT_ERROR',
    message: error.message
  }

  if (error instanceof ValidationError) {
    errorObject.status = 400
    errorObject.code = 'VALIDATION_ERROR'
    errorObject.message = error.message
  } else if (isHttpError(error)) {
    errorObject.status =  error.statusCode
    errorObject.code = error.constructor.name
    errorObject.message = error.message
  // } else {
  //   const err = errors.find((err) => err.code === error.message);

  //   if (err) {
  //     errorObject.status =  err.status
  //     errorObject.code = err.code
  //     errorObject.message = err.message
  //   }
  }

  logger.error(error);

  return reply.status(errorObject.status).send({
    status: errorObject.status,
    code: errorObject.code,
    message: errorObject.message
  });
};
