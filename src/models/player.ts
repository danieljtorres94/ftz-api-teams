import { model, Schema, Model, Document } from 'mongoose';

export interface ICountry {
  nombre: string;
  paisId: number;
}

export interface IRole {
  nombre: string;
  idRol: number;
}

export interface IClub {
  id: number;
  nombre: string;
  paisId: number;
  paisNombre: string;
  paisSigla: string;
  tipo: string;
}

export interface IPlayer extends Document {
  id: number;
  nombre: string;
  apellido: string;
  nombreCorto: string;
  ladoHabil: string;
  fechaNacimiento: string;
  horaNacimiento: string;
  edad: number;
  peso: number;
  altura: number;
  apodo: string;
  rol: IRole;
  camiseta: number;
  pais: ICountry;
  provincia: string;
  clubActual: IClub;
  localidad: string;
}

const PlayerSchema: Schema = new Schema({
  id:  { type: Number },
  idEquipo:  { type: Number },
  dadoBaja:  { type: Boolean },
  nombre: { type: String },
  apellido: { type: String },
  nombreCorto: { type: String },
  ladoHabil: { type: String },
  fechaNacimiento: { type: String },
  horaNacimiento: { type: String },
  edad: { type: Number },
  peso: { type: Number },
  altura: { type: Number },
  apodo: { type: String },
  rol: { nombre: { type: String }, idRol: { type: Number } },
  camiseta: { type: Number },
  pais: { nombre: { type: String }, paisId: { type: Number } },
  provincia: { type: String },
  clubActual: {
    nombre: { type: String },
    paisId: { type: Number },
    paisNombre: { type: String },
    paisSigla: { type: String },
    tipo: { type: String }
  },
  localidad: { type: String }
});

const Player: Model<IPlayer> = model('Player', PlayerSchema);

export default Player;
