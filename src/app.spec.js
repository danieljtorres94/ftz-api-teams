'use strict';
const app = require('../dist/app');

describe('HealthCheck', () => {
  test('/health reply with status with 200', async () => {
    const server = await app.default({});
    const res = await server.inject({
      method: 'GET',
      url: '/api/health'
    });
    expect(res.statusCode).toBe(200);
    // expect(res.json().statusCode).toBe(200)
    // expect(res.json().status).toBe('OK')
  });
});
