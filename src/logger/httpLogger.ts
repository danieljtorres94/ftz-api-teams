import morgan from 'morgan';
import morganJson from 'morgan-json';
import logger from './index';

const format = morganJson({
  method: ':method',
  url: ':url',
  status: ':status',
  contentLength: ':res[content-length]',
  responseTime: ':response-time',
  query: ':req[query]'
});

export default morgan(format, {
  stream: {
    write: (message) => {
      const { method, url, status, contentLength, responseTime, query } = JSON.parse(
        message
      );

      logger.info('HTTP Logs', {
        method,
        url,
        status: Number(status),
        contentLength,
        responseTime: Number(responseTime)
      });
    }
  }
});
