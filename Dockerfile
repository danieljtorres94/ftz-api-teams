FROM node:14.16.0-alpine3.10
USER root

RUN npm i -g pm2

ENV HOME /home/node/app
WORKDIR $HOME
COPY . .

RUN npm ci
RUN npm run build

ENV PORT 8080
EXPOSE $PORT

RUN chown -R 1001:0 "/home/node/app"
RUN chmod -R ug+rw "/home/node/app"

USER 1001

CMD ["pm2-runtime", "./dist/index.js"]