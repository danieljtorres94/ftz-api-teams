export default {
  audience: 'https://danieljtorres.auth0.com/api/v2/',
  issuer: `https://danieljtorres.auth0.com/`,
  domain: `https://danieljtorres.auth0.com/`,
}