import mongoose from 'mongoose';

const {
  HAVE_AUTH,
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_HOSTNAME,
  MONGO_PORT,
  MONGO_DB
} = process.env;

const options = {
  useNewUrlParser: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 500,
  connectTimeoutMS: 10000
};

let connectionUrl = '';

if (HAVE_AUTH) {
  connectionUrl = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?replicaSet=rs0`;
} else {
  connectionUrl = `mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`;
}

export default async () => {
  try {
    await mongoose.connect(connectionUrl, options);
    console.log('Database connection successful');
  } catch (error) {
    console.error('Database connection error');
    throw error;
  }
};
