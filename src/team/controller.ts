import { FastifyRequest, FastifyReply } from 'fastify';
import qs from 'qs'
import logger from '../logger';

import models from '../models';
import { makeGetAllTeams, makeGetAllTeamPlayers, makeGetAllTeamPlayersByPosition } from './service';
import validator from './validator';
import { prepareQuery } from '../helpers';

const getAllTeams = makeGetAllTeams({
  models,
  logger,
});

const getAllTeamPlayers = makeGetAllTeamPlayers({
  models,
  logger,
});

const getAllTeamPlayersByPosotion = makeGetAllTeamPlayersByPosition({
  models,
  logger,
});

export const getTeams = async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  try {  
    const { query } = request;

    const { filter, sort, page = { skip: 0, limit: 5 } } = qs.parse(query);

    await validator.teamQuerySchema.validateAsync({ filter, sort, page });

    const preparedQuery = prepareQuery(filter , sort)

    const { teams } = await getAllTeams({ ...preparedQuery , page });

    reply.send({ result: teams });
  } catch (error) {
    throw error
  }
};

export const getTeamPlayers = async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  try {  
    const { query, params }: any = request;

    const { filter, sort, page = { skip: 0, limit: 5 } } = qs.parse(query);

    await validator.playerQuerySchema.validateAsync({ filter, sort, page });

    const preparedQuery = prepareQuery(filter , sort)

    const { players } = await getAllTeamPlayers(params.teamId, { ...preparedQuery , page });

    reply.send({ result: players });
  } catch (error) {
    throw error
  }
};

export const getTeamPlayersByPosition = async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  try {  
    const { query, params }: any = request;

    const { filter, sort, page = { skip: 0, limit: 5 } } = qs.parse(query);

    await validator.playerQuerySchema.validateAsync({ filter, sort, page });

    const preparedQuery = prepareQuery(filter , sort)

    const { players } = await getAllTeamPlayersByPosotion(params.position, { ...preparedQuery , page });

    reply.send({ result: players });
  } catch (error) {
    throw error
  }
};