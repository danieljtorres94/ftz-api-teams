# FTZ API TEAMS

API para la consulta de equipos de futbol y sus jugadores.

Diagrama de Componentes: (https://drive.google.com/file/d/1wVWrSw-jFEkDe-xtSWQ101tkv1O23DMD/view?usp=sharing)

URL: http://35.198.25.249/api

## Requisitos previos
- Docker
- Kubectl
- Istio Service Mesh (https://istio.io/latest/docs/setup/getting-started/)
- Proyecto https://gitlab.com/danieljtorres94/ftz-preload-mongo previamente lanzado

## Lanzar proyecto (Kubernetes)
- Ejecutar desde la terminal en la ruta del proyecto:
```
$ kubectl create -f ./k8s/prod.yml
```

- Limpieza:
```
$ kubectl delete -f ./k8s/prod.yml
```
## Lanzar proyecto (Docker)
- Ejecutar desde la terminal en la ruta del proyecto:
```
$ docker build -t {image_name} .
$ docker run {image_name} -p 3000:3000
```

## Documentación

### **Rutas**
   -  ```{BASE_URL}/team```
   -  ```{BASE_URL}/team/:idTeam/player```
   -  ```{BASE_URL}/team/player/:position```

### **Obtener Access Token**
  
  Con el siguiente CURL se obtiene el access token para enviar en el header ```Authorization: Bearer {Access_Token}```
```
 curl --request POST --url https://danieljtorres.auth0.com/oauth/token --header 'content-type: application/json' --data '{"client_id":"NIW0MRQoKsmDU0XOGtKHvZCd8tj0bPF6","client_secret":"7J8Dh3zTaxlg1ynLRs8mTKTtYq9RPsp-nMz1ZJsh2K1lB5KovGc4dNORstoCvAUB","audience":"https://danieljtorres.auth0.com/api/v2/","grant_type":"client_credentials"}'
```

### **Query Params**
  
  Existen tres parámetros posibles para filtrar: ```filter, sort, page```
  
#### Filter  (Filtering)
El valor de filter sirve para filtrar mediante campos y operadores de comparación:
Ejemplo:
```
?filter[name]=value
?filter[name][operator]=value
```
 - `name`: Es el nombre del campo con el que se quiere filtrar
- `value`: Puede ser un string, numero, array o null, si es array tiene que ser un operator que soporte arrays
- `operator`: Es el operador a aplicar al filtro, es opcional, si no se especifica es equivalente a ```=```

**operadores soportados**: 
-   `=`: string, number, or boolean
-   `>=`: string or number
-   `>`: string or number
-   `<`: string or number
-   `<=`: string or number
-   `in`: array of strings and/or numbers
-   `not in`: array of strings and/or numbers

#### Sort (Sorting)

```
?sort=name
?sort[name]=order
```
-   `order`  puede ser  `asc`  o  `desc`  (case-insensitive), y por default es  `asc`.
-   `name` es el nombre del campo por el cual se hara el ordenamiento.

#### Page (Pagination)
```
?page[skip]=value&page[limit]=value
```
-   `skip`  es opcional y por default 0.
-   `limit` es opcional y por default es 5.


### Ejemplo QUERY

```
/api/team?filter[id]=143&filter[sigla]=AUD&sort[id]=desc&page[skip]=0&page[limit]=4
```