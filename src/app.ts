import { fastify, FastifyRegisterOptions, FastifyServerOptions } from 'fastify';
import fastifyBlipp from 'fastify-blipp';
import fastifyCors from 'fastify-cors';
import helmet from 'fastify-helmet';
import swagger from 'fastify-swagger';
import fastifyJwt from 'fastify-auth0-verify';
import fastifyExpress from 'fastify-express';
import httpLogger from './logger/httpLogger';
import errorHandler from './errors';
import jwtConfig from './jwt';
import swaggerCSP from '../node_modules/fastify-swagger/static/csp.json'

import teamRoutes from './team';

export default async (FASTIFY_CONFIG: FastifyServerOptions) => {
  const app = fastify(FASTIFY_CONFIG);

  await app.register(fastifyExpress);
  //@ts-ignore
  app.use(httpLogger);

  // app.decorate('swaggerCSP', require('../node_modules/fastify-swagger/static/csp.json'))

  app.register(fastifyBlipp);

  app.register(fastifyCors, {
    methods: ['GET'],
    allowedHeaders: ['Content-Type', 'Authorization'],
    credentials: true
  });

  app.register(swagger, {
    routePrefix: '/docs',
    exposeRoute: true
  });

  //@ts-ignore
  app.register(helmet, (instance: any) => {
    return {
      contentSecurityPolicy: {
        directives: {
            //@ts-ignore
          ...helmet.contentSecurityPolicy.getDefaultDirectives(),
          "form-action": ["'self'"],
          "img-src": ["'self'", "data:", "validator.swagger.io"],
          "script-src": ["'self'"].concat(swaggerCSP.script),
          "style-src": ["'self'", "https:"].concat(
            swaggerCSP.style
          ),
        }
      }
    }
  })

  app.register(fastifyJwt, jwtConfig);

  app.register(routes, { prefix: '/api' });

  app.addHook('onRequest', async (request, reply) => {
    //Interceptors
  });

  app.setErrorHandler(errorHandler);

  return app;
};

function routes(fastify, opts, done) {
  fastify.route({
    method: 'GET',
    url: '/health',
    logLevel: 'debug',
    handler: async (request, reply) => {
      reply.status(200).send('ping pong');
    }
  });

  teamRoutes.getTeams.preValidation = [fastify.authenticate];
  teamRoutes.getTeamPlayers.preValidation = [fastify.authenticate];
  teamRoutes.getTeamPlayersByPosition.preValidation = [fastify.authenticate];

  fastify.route(teamRoutes.getTeams);
  fastify.route(teamRoutes.getTeamPlayers);
  fastify.route(teamRoutes.getTeamPlayersByPosition)
  done();
}
