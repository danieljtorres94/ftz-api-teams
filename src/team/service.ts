export const makeGetAllTeams = ({ models: { Team }, logger }) => {
  return async (query) => {
    try {
      const { filter, sort, page } = query;

      const dbQuery = Team.find(filter);
      dbQuery.sort(sort);
      dbQuery.limit(parseInt(page.limit));
      dbQuery.skip(parseInt(page.skip));

      return {
        teams: await dbQuery
      };
    } catch (error) {
      throw error;
    }
  };
};

export const makeGetAllTeamPlayers = ({ models: { Player }, logger }) => {
  return async (idTeam, query) => {
    try {
      const { filter, sort, page } = query;

      const dbQuery = Player.find({ ...filter, idEquipo: idTeam });
      dbQuery.sort(sort);
      dbQuery.limit(parseInt(page.limit));
      dbQuery.skip(parseInt(page.skip));

      return {
        players: await dbQuery
      };
    } catch (error) {
      throw error;
    }
  };
};

export const makeGetAllTeamPlayersByPosition = ({
  models: { Player },
  logger
}) => {
  return async (position, query) => {
    try {
      const { filter, sort, page } = query;

      const dbQuery = Player.aggregate([
        {
          $lookup: {
            from: 'teams',
            localField: 'idEquipo',
            foreignField: 'id',
            as: 'teamData'
          }
        },
        {
          $unwind: '$teamData'
        },
        {
          $match: { 'rol.nombre': position.charAt(0).toUpperCase() + position.slice(1), ...filter }
        },
        {
          $sort: Object.keys(sort).length ? sort : { id: -1 }
        },
        {
          $group: { _id: '$idEquipo', nombre: { $first: '$teamData.nombre' }, players: { $push: '$$ROOT' } }
        },
        { 
          $addFields: { id: "$_id" }
        },
        {
          $project: { 'players.__v': 0, 'players.teamData': 0, _id: 0, 'players._id': 0 }
        },
        {
          $sort: { id: -1 }
        }
      ]);
      dbQuery.limit(parseInt(page.limit));
      dbQuery.skip(parseInt(page.skip));

      return {
        players: await dbQuery
      };
    } catch (error) {
      throw error;
    }
  };
};
