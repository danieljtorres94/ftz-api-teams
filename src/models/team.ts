import { model, Schema, Model, Document } from 'mongoose';

export interface IChannelInfo {
  nombre: string;
  id: number;
  canal: string;
}

export type ISport = IChannelInfo;

export type ICategory = IChannelInfo;

export type IChampionship = IChannelInfo;

export type IChampionshipAltName = IChannelInfo;

export interface ITeam extends Document {
  id: number;
  nombre: string;
  sigla: string;
  paisId: number;
  paisNombre: string;
  tipo: string;
  jugadores: Number;
  jugadoresDadosBaja: Number;
  deporte: ISport;
  categoria: ICategory;
  campeonato: IChampionship;
  campeonatoNombreAlternativo: IChampionshipAltName;
  fechaActual: string;
}

const TeamSchema: Schema = new Schema({
  id: Number,
  nombre: String,
  sigla: String,
  paisId: Number,
  paisNombre: String,
  tipo: String,
  jugadores: Number,
  jugadoresDadosBaja: Number,
  deporte: {
    nombre: { type: String },
    id: { type: Number },
    canal: { type: String }
  },
  categoria: {
    nombre: { type: String },
    id: { type: Number },
    canal: { type: String }
  },
  campeonato: {
    nombre: { type: String },
    id: { type: Number },
    canal: { type: String }
  },
  campeonatoNombreAlternativo: {
    nombre: { type: String },
    id: { type: Number },
    canal: { type: String }
  },
  fechaActual: String
});

const Team: Model<ITeam> = model('Team', TeamSchema);

export default Team;
