export const prepareQuery = (filter: any, sort: any) => {
  const preparedQuery = { sort: {}, filter: {} };

  if (filter) {
    const filterKeys = Object.keys(filter);
    filterKeys.forEach((fkey) => {
      if (typeof filter[fkey] === 'string') {
        preparedQuery['filter'][fkey] = filter[fkey]
      } else {
        const itemKeys = Object.keys(filter[fkey])
        itemKeys.forEach((ikey) => {
          const operator = operatorsMap.get(ikey);
          if (operator) {
            preparedQuery['filter'][fkey] = { [operator]: filter[fkey][ikey] }
          }
        })
      }
    });
  }

  if (sort) {
    if (typeof sort === 'string') { 
      preparedQuery['sort'][sort] = -1
    } else {
      const sortKeys = Object.keys(sort);
      sortKeys.forEach((skey) => {
        preparedQuery['sort'][skey] = sort[skey] === 'asc' ? 1 : -1
      });
    }
  }

  return preparedQuery;
}

export const operatorsMap = new Map([
  ['=', '$eq'],
  ['>', '$gt'],
  ['>=', '$gte'],
  ['in', '$in'],
  ['<', '$lt'],
  ['<=', '$lte'],
  ['not_in', '$ne']
]);
