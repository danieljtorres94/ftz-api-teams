import 'dotenv/config';

import app from './app';
import connectDB from './database';

export const bootstrap = async () => {
  try {
    await connectDB();
    const server = await app({});
    const address = await server.listen(3000, '0.0.0.0');
    server.blipp();
    console.log(`Server listenig on: ${address}`);
  } catch (error) {
    console.error(error);
    process.exit(1);
  }
};
bootstrap();
