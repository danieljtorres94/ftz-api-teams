import winston from 'winston';

const { combine, timestamp, printf, prettyPrint, metadata } = winston.format;

const logFormat = printf(({ level, message, timestamp, ...meta }) => {
  return JSON.stringify({
    timestamp,
    level,
    message,
    ...meta
  });
});

const options = {
  level: 'debug',
  levels: winston.config.npm.levels,
  // format: winston.format.json(),
  handleExceptions: true,
  json: false,
  colorize: true
};

export default winston.createLogger({
  levels: winston.config.npm.levels,
  format: combine(timestamp(), prettyPrint(), logFormat),
  transports: [new winston.transports.Console(options)],
  exitOnError: false
});
