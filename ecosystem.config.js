module.exports = {
  apps: [
    {
      name: 'main.api',
      script: './dist/index.js',
      exec_mode: 'cluster',
      instances: -1
    }
  ]
}