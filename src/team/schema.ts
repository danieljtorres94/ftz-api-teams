export const getTeamsSchema = {
  querystring: {
  },
  response: {
    200: {
      type: 'object',
      properties: {
        result: { 
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id: { type: 'integer' },
              nombre: { type: 'string' },
              sigla: { type: 'string' },
              paisId: { type: 'integer' },
              paisNombre: { type: 'string' },
              tipo: { type: 'string' },
              jugadores: { type: 'integer' },
              jugadoresDadosBaja: { type: 'integer' },
              deporte: {
                nombre: { type: { type: 'string' } },
                id: { type: { type: 'integer' } },
                canal: { type: { type: 'string' } }
              },
              categoria: {
                nombre: { type: { type: 'string' } },
                id: { type: { type: 'integer' } },
                canal: { type: { type: 'string' } }
              },
              campeonato: {
                nombre: { type: { type: 'string' } },
                id: { type: { type: 'integer' } },
                canal: { type: { type: 'string' } }
              },
              campeonatoNombreAlternativo: {
                nombre: { type: { type: 'string' } },
                id: { type: { type: 'integer' } },
                canal: { type: { type: 'string' } }
              },
              fechaActual: { type: 'string' }
            }
          }
        }
      }
    }
  }
};


export const getTeamPlayersSchema = {
  querystring: {
  },
  response: {
    200: {
      type: 'object',
      properties: {
        result: { 
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id:  { type: 'integer' },
              idEquipo:  { type: 'integer' },
              dadoBaja:  { type: 'boolean' },
              nombre: { type: 'string' },
              apellido: { type: 'string' },
              nombreCorto: { type: 'string' },
              ladoHabil: { type: 'string' },
              fechaNacimiento: { type: 'string' },
              horaNacimiento: { type: 'string' },
              edad: { type: 'integer' },
              peso: { type: 'integer' },
              altura: { type: 'integer' },
              apodo: { type: 'string' },
              rol: { nombre: { type: 'string' }, idRol: { type: 'integer' } },
              camiseta: { type: 'integer' },
              pais: { nombre: { type: 'string' }, paisId: { type: 'integer' } },
              provincia: { type: 'string' },
              clubActual: {
                nombre: { type: 'string' },
                paisId: { type: 'integer' },
                paisNombre: { type: 'string' },
                paisSigla: { type: 'string' },
                tipo: { type: 'string' }
              },
              localidad: { type: 'string' }
            }
          }
        }
      }
    }
  }
};


export const getTeamPlayersByPositionSchema = {
  querystring: {
  },
  response: {
    200: {
      type: 'object',
      properties: {
        result: { 
          type: 'array',
          items: {
            type: 'object',
            properties: {
              id:  { type: 'integer' },
              nombre: { type: 'string' },
              players: {
                type: 'array',
                items: {
                  type: 'object',
                  properties: {
                    id:  { type: 'integer' },
                    idEquipo:  { type: 'integer' },
                    dadoBaja:  { type: 'boolean' },
                    nombre: { type: 'string' },
                    apellido: { type: 'string' },
                    nombreCorto: { type: 'string' },
                    ladoHabil: { type: 'string' },
                    fechaNacimiento: { type: 'string' },
                    horaNacimiento: { type: 'string' },
                    edad: { type: 'integer' },
                    peso: { type: 'integer' },
                    altura: { type: 'integer' },
                    apodo: { type: 'string' },
                    rol: { nombre: { type: 'string' }, idRol: { type: 'integer' } },
                    camiseta: { type: 'integer' },
                    pais: { nombre: { type: 'string' }, paisId: { type: 'integer' } },
                    provincia: { type: 'string' },
                    clubActual: {
                      nombre: { type: 'string' },
                      paisId: { type: 'integer' },
                      paisNombre: { type: 'string' },
                      paisSigla: { type: 'string' },
                      tipo: { type: 'string' }
                    },
                    localidad: { type: 'string' }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
};
