import Joi from 'joi';

const filterOperatorsSchema = Joi.object({
  '=': Joi.alternatives().try(
    Joi.number().integer(),
    Joi.string(),
    Joi.boolean()
  ),
  '!=': Joi.alternatives().try(
    Joi.number().integer(),
    Joi.string(),
    Joi.boolean()
  ),
  '<>': Joi.alternatives().try(Joi.number().integer(), Joi.string()),
  '>': Joi.alternatives().try(Joi.number().integer(), Joi.string()),
  '>=': Joi.alternatives().try(Joi.number().integer(), Joi.string()),
  '<': Joi.alternatives().try(Joi.number().integer(), Joi.string()),
  '<=': Joi.alternatives().try(Joi.number().integer(), Joi.string()),
  in: Joi.array()
    .items(Joi.alternatives().try(Joi.number().integer(), Joi.string()))
    .min(2)
    .max(2),
  not_in: Joi.array()
    .items(Joi.alternatives().try(Joi.number().integer(), Joi.string()))
    .min(2)
    .max(2),
  between: Joi.array()
    .items(Joi.alternatives().try(Joi.number().integer(), Joi.string()))
    .min(2)
    .max(2),
  not_between: Joi.array()
    .items(Joi.alternatives().try(Joi.number().integer(), Joi.string()))
    .min(2)
    .max(2)
}).nand(
  '=',
  '!=',
  '<>',
  '>',
  '>=',
  '<',
  '<=',
  'in',
  'not_in',
  'like',
  'ilike',
  'between',
  'not_between'
);

const pageSchema = Joi.object().keys({
  limit: Joi.number().integer(),
  skip: Joi.number().integer()
});

// teamsFilters and Sort schemas
const teamFilterSchema = Joi.object().keys({
  id: Joi.alternatives().try(Joi.number().integer(), filterOperatorsSchema),
  'deporte.id': Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  'deporte.nombre': Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  'deporte.canal': Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  'categoria.id': Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  'categoria.nombre': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'categoria.canal': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'campeonato.id': Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  'campeonato.nombre': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'campeonato.canal': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'campeonatoNombreAlternativo.id': Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  'campeonatoNombreAlternativo.nombre': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'campeonatoNombreAlternativo.canal': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  jugadores: Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  jugadoresDadosBaja: Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  nombre: Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  sigla: Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  paisId: Joi.alternatives().try(Joi.number().integer(), filterOperatorsSchema),
  paisNombre: Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  tipo: Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  fechaActual: Joi.alternatives().try(Joi.string(), filterOperatorsSchema)
});

const teamSortSchema = Joi.object()
  .keys({
    id: Joi.string().valid('asc', 'desc'),
    'deporte.id': Joi.string().valid('asc', 'desc'),
    'deporte.nombre': Joi.string().valid('asc', 'desc'),
    'deporte.canal': Joi.string().valid('asc', 'desc'),
    'categoria.id': Joi.string().valid('asc', 'desc'),
    'categoria.nombre': Joi.string().valid('asc', 'desc'),
    'categoria.canal': Joi.string().valid('asc', 'desc'),
    'campeonato.id': Joi.string().valid('asc', 'desc'),
    'campeonato.nombre': Joi.string().valid('asc', 'desc'),
    'campeonato.canal': Joi.string().valid('asc', 'desc'),
    'campeonatoNombreAlternativo.id': Joi.string().valid('asc', 'desc'),
    'campeonatoNombreAlternativo.nombre': Joi.string().valid('asc', 'desc'),
    'campeonatoNombreAlternativo.canal': Joi.string().valid('asc', 'desc'),
    jugadores: Joi.string().valid('asc', 'desc'),
    jugadoresDadosBaja: Joi.string().valid('asc', 'desc'),
    nombre: Joi.string().valid('asc', 'desc'),
    sigla: Joi.string().valid('asc', 'desc'),
    paisId:Joi.string().valid('asc', 'desc'),
    paisNombre: Joi.string().valid('asc', 'desc'),
    tipo: Joi.string().valid('asc', 'desc'),
    fechaActual: Joi.string().valid('asc', 'desc')
  })
  .nand(
    'id',
    'deporte.id',
    'deporte.nombre',
    'deporte.canal',
    'categoria.id',
    'categoria.nombre',
    'categoria.canal',
    'campeonato.id',
    'campeonato.nombre',
    'campeonato.canal',
    'campeonatoNombreAlternativo.id',
    'campeonatoNombreAlternativo.nombre',
    'campeonatoNombreAlternativo.canal',
    'jugadores',
    'jugadoresDadosBaja',
    'nombre',
    'sigla',
    'paisId',
    'paisNombre',
    'tipo',
    'fechaActual'
  );

const teamQuerySchema = Joi.object({
  filter: teamFilterSchema,
  sort: Joi.alternatives().try(
    Joi.string().valid(
      'id',
      'deporte.id',
      'deporte.nombre',
      'deporte.canal',
      'categoria.id',
      'categoria.nombre',
      'categoria.canal',
      'campeonato.id',
      'campeonato.nombre',
      'campeonato.canal',
      'campeonatoNombreAlternativo.id',
      'campeonatoNombreAlternativo.nombre',
      'campeonatoNombreAlternativo.canal',
      'jugadores',
      'jugadoresDadosBaja',
      'nombre',
      'sigla',
      'paisId',
      'paisNombre',
      'tipo',
      'fechaActual'
    ),
    teamSortSchema
  ),
  page: pageSchema
});

// playersFilters and Sort schemas
const playerFilterSchema = Joi.object().keys({
  id: Joi.alternatives().try(Joi.number().integer(), filterOperatorsSchema),
  'deporte.id': Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  'deporte.nombre': Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  'deporte.canal': Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  'edad': Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  'categoria.nombre': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'categoria.canal': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'campeonato.id': Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  'campeonato.nombre': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'campeonato.canal': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'campeonatoNombreAlternativo.id': Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  'campeonatoNombreAlternativo.nombre': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  'campeonatoNombreAlternativo.canal': Joi.alternatives().try(
    Joi.string(),
    filterOperatorsSchema
  ),
  jugadores: Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  jugadoresDadosBaja: Joi.alternatives().try(
    Joi.number().integer(),
    filterOperatorsSchema
  ),
  nombre: Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  sigla: Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  paisId: Joi.alternatives().try(Joi.number().integer(), filterOperatorsSchema),
  paisNombre: Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  tipo: Joi.alternatives().try(Joi.string(), filterOperatorsSchema),
  fechaActual: Joi.alternatives().try(Joi.string(), filterOperatorsSchema)
});

const playerSortSchema = Joi.object()
  .keys({
    'id': Joi.string().valid('asc', 'desc'),
    'idEquipo': Joi.string().valid('asc', 'desc'),
    'dadoBaja': Joi.string().valid('asc', 'desc'),
    'nombre': Joi.string().valid('asc', 'desc'),
    'apellido': Joi.string().valid('asc', 'desc'),
    'nombreCorto': Joi.string().valid('asc', 'desc'),
    'ladoHabil': Joi.string().valid('asc', 'desc'),
    'fechaNacimiento': Joi.string().valid('asc', 'desc'),
    'horaNacimiento': Joi.string().valid('asc', 'desc'),
    'edad': Joi.string().valid('asc', 'desc'),
    'peso': Joi.string().valid('asc', 'desc'),
    'altura': Joi.string().valid('asc', 'desc'),
    'apodo': Joi.string().valid('asc', 'desc'),
    'rol.nombre': Joi.string().valid('asc', 'desc'),
    'rol.idRol': Joi.string().valid('asc', 'desc'),
    'camiseta': Joi.string().valid('asc', 'desc'),
    'pais.nombre': Joi.string().valid('asc', 'desc'),
    'pais.paisId': Joi.string().valid('asc', 'desc'),
    'provincia': Joi.string().valid('asc', 'desc'),
    'clubActual.nombre': Joi.string().valid('asc', 'desc'),
    'clubActual.paisId': Joi.string().valid('asc', 'desc'),
    'clubActual.paisNombre': Joi.string().valid('asc', 'desc'),
    'clubActual.paisSigla': Joi.string().valid('asc', 'desc'),
    'clubActual.tipo': Joi.string().valid('asc', 'desc'),
    'localidad': Joi.string().valid('asc', 'desc')
  })
  .nand(
    'id',
    'idEquipo',
    'dadoBaja',
    'nombre',
    'apellido',
    'nombreCorto',
    'ladoHabil',
    'fechaNacimiento',
    'horaNacimiento',
    'edad',
    'peso',
    'altura',
    'apodo',
    'rol.nombre',
    'rol.idRol',
    'camiseta',
    'pais.nombre',
    'pais.paisId',
    'provincia',
    'clubActual.nombre',
    'clubActual.paisId',
    'clubActual.paisNombre',
    'clubActual.paisSigla',
    'clubActual.tipo',
    'localidad'
  );

const playerQuerySchema = Joi.object({
  filter: playerFilterSchema,
  sort: Joi.alternatives().try(
    Joi.string().valid(
      'id',
      'idEquipo',
      'dadoBaja',
      'nombre',
      'apellido',
      'nombreCorto',
      'ladoHabil',
      'fechaNacimiento',
      'horaNacimiento',
      'edad',
      'peso',
      'altura',
      'apodo',
      'rol.nombre',
      'rol.idRol',
      'camiseta',
      'pais.nombre',
      'pais.paisId',
      'provincia',
      'clubActual.nombre',
      'clubActual.paisId',
      'clubActual.paisNombre',
      'clubActual.paisSigla',
      'clubActual.tipo',
      'localidad'
    ),
    playerSortSchema
  ),
  page: pageSchema
});

export default {
  teamQuerySchema,
  playerQuerySchema
};
