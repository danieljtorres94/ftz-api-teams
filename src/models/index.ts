import Team from './team';
import Player from './player';

export default {
  Team,
  Player
};
