import { getTeamsSchema, getTeamPlayersSchema, getTeamPlayersByPositionSchema } from './schema';
import { getTeams as getTeamsHandler, getTeamPlayers as getTeamPlayersHandler, getTeamPlayersByPosition as getTeamPlayersByPositionHandler } from './controller';
import { RouteOptions } from 'fastify';

const getTeams: RouteOptions = {
  method: 'GET',
  url: '/team',
  schema: getTeamsSchema,
  handler: getTeamsHandler
};

const getTeamPlayers: RouteOptions = {
  method: 'GET',
  url: '/team/:teamId/players',
  schema: getTeamPlayersSchema,
  handler: getTeamPlayersHandler
};

const getTeamPlayersByPosition: RouteOptions = {
  method: 'GET',
  url: '/team/players/:position',
  schema: getTeamPlayersByPositionSchema,
  handler: getTeamPlayersByPositionHandler
};

export default {
  getTeams,
  getTeamPlayers,
  getTeamPlayersByPosition
};
